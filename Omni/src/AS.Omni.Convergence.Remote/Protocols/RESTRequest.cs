﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AS.Omni.Convergence.Remote.Protocols
{
    using Interfaces;
    using Models;
    public class RESTRequest : IServiceRequest<JObject>
    {
        public static RemoteType RemoteType => RemoteType.REST;
        public string Command { get; set; }
    }

    internal class RESTService : ServiceRequestHandler<RESTRequest, JObject>
    {
        private HttpClient _httpClient;
        //private IAuthHandler _authHandler;

        //public RESTService(IAuthHandler authHandler)
        public RESTService()
        {
            _httpClient = new HttpClient();
            //_httpClient = new HttpClient(new LoggingHandler(new HttpClientHandler()));
            //_authHandler = authHandler;
        }

        protected override async Task<JObject> Execute(string command, IRemote remote)
        {
            _httpClient.BaseAddress = remote.BaseAddress;
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(remote.MediaType));

            // await _authHandler.ExecuteAuthentication(_provider, _httpClient);

            HttpResponseMessage response;
            switch (remote.HttpVerb)
            {
                case HttpVerb.GET:
                    response = await _httpClient.GetAsync($"{remote.RequestUri}{command}");
                    break;
                case HttpVerb.PUT:
                    response = await _httpClient.PutAsync(remote.RequestUri, new StringContent(command, Encoding.UTF8, remote.MediaType));
                    break;
                case HttpVerb.POST:
                default:
                    response = await _httpClient.PostAsync(remote.RequestUri, new StringContent(command, Encoding.UTF8, remote.MediaType));
                    break;

            }
            response.EnsureSuccessStatusCode();

            string responsestring = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<JObject>(responsestring);

        }
    }


}
