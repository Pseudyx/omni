﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace AS.Omni.Convergence.Remote
{
    using Models;
    public class RequestRegister
    {
        private static readonly ConcurrentDictionary<RemoteType, Type> _providerRequests = new ConcurrentDictionary<RemoteType, Type>();

        public Type Get(RemoteType remote) => _providerRequests[remote];
        public Type GetOrAdd(RemoteType remote, Type type) => _providerRequests.GetOrAdd(remote, type);
        public Type AddOrUpdate(RemoteType remote, Type type) => _providerRequests.AddOrUpdate(remote, type, (key, oldValue) => type);
        public IEnumerator<KeyValuePair<RemoteType, Type>> ToList() => _providerRequests.GetEnumerator();

        public RequestRegister()
        {
            AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(assemply => assemply.GetTypes()
                                                .Where(type => type.IsClass))
                .ForEach(type => {
                    type.GetInterfaces()
                        .Where(typeInterface => typeInterface.IsGenericType
                                             && (typeInterface.GetGenericTypeDefinition() == typeof(IServiceRequest<>).GetGenericTypeDefinition()))
                    .ForEach(typeInterface =>
                    {
                        var remote = type.GetProperty("RemoteType")?.GetValue(Activator.CreateInstance(type));
                        if (remote != null)
                        {
                            _providerRequests.AddOrUpdate((RemoteType)remote, type, (key, oldValue) => type);
                        }
                    });
                });


        }
    }
}
