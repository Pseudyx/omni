﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace AS.Omni.Convergence.Remote
{
    using AS.Omni.Convergence.Remote.Internal;
    using AS.Omni.Interfaces;
    using Broker;
    using System.Threading;
    using System.Threading.Tasks;

    public class ServicesFactory : IServiceFactory
    {
        private readonly ServiceFactory _serviceFactory;
        private readonly RequestRegister _requestsRegister;
        private static readonly ConcurrentDictionary<Type, object> _requestHandlers = new ConcurrentDictionary<Type, object>();

        public ServicesFactory(ServiceFactory serviceFactory, RequestRegister requestRegister)
        {
            _serviceFactory = serviceFactory;
            _requestsRegister = requestRegister;

        }

        public Task<TResponse> Send<TResponse, TProvider>(IRemoteRequest<TResponse, TProvider> request, CancellationToken cancellationToken) where TProvider : IRemote
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }
            if (request.Remote == null)
            {
                throw new ArgumentNullException(nameof(request.Remote));
            }

            var requestType = _requestsRegister.Get(request.Remote.Type);

            var handler = (RequestHandlerWrapper<TResponse>)_requestHandlers.GetOrAdd(requestType,
                tx => Activator.CreateInstance(typeof(RequestHandlerImplementation<,>).MakeGenericType(requestType, typeof(TResponse))));

            return handler.Execute(request.Request.Command, request.Remote, cancellationToken, _serviceFactory);
        }
    }
}
