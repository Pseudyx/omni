﻿using System.Threading;
using System.Threading.Tasks;

namespace AS.Omni.Convergence.Remote
{
    using Interfaces;
    public interface IServiceFactory
    {
        Task<TResponse> Send<TResponse, TRemote>(IRemoteRequest<TResponse, TRemote> request, CancellationToken cancellationToken) where TRemote : IRemote;
    }
}
