﻿using AS.Omni.Broker;
using AS.Omni.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AS.Omni.Convergence.Remote.Internal
{
    internal abstract class RequestHandlerBase
    {
        protected static THandler GetHandler<THandler>(ServiceFactory factory)
        {
            THandler handler;

            try
            {
                handler = factory.GetInstance<THandler>();
            }
            catch (Exception e)
            {
                throw new InvalidOperationException($"Error constructing handler for request of type {typeof(THandler)}. Register your handlers with the container.", e);
            }

            if (handler == null)
            {
                throw new InvalidOperationException($"Handler was not found for request of type {typeof(THandler)}. Register your handlers with the container.");
            }

            return handler;
        }
    }

    internal abstract class RequestHandlerWrapper<TResponse> : RequestHandlerBase
    {
        public abstract Task<TResponse> Execute(string command, IRemote remote, CancellationToken cancellationToken,
            ServiceFactory serviceFactory);
    }

    internal class RequestHandlerImplementation<TRequest, TResponse> : RequestHandlerWrapper<TResponse>
        where TRequest : IServiceRequest<TResponse>
    {
        public override Task<TResponse> Execute(string command, IRemote remote, CancellationToken cancellationToken,
            ServiceFactory serviceFactory)
        {
            Task<TResponse> Handler() => GetHandler<IServiceRequestHandler<TRequest, TResponse>>(serviceFactory).Execute(command, remote, cancellationToken);

            return Handler();
        }
    }
}
