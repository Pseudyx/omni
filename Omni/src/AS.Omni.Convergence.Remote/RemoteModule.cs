﻿using Autofac;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace AS.Omni.Convergence.Remote
{
    public class RemoteModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RequestRegister>().AsSelf().SingleInstance();

            // Register service factory and all the servic Command classes
            builder.RegisterAssemblyTypes(typeof(IServiceFactory).GetTypeInfo().Assembly)
                .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(
                                 typeof(IServiceRequest<>).GetTypeInfo().Assembly).
                                      AsClosedTypesOf(typeof(IServiceRequestHandler<,>));


            // Other types registration
            //...
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).AsImplementedInterfaces();

            

        }
    }
}
