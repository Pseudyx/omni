﻿using AS.Omni.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AS.Omni.Convergence.Remote
{
    public interface IServiceRequest<out TResponse>
    {
        string Command { get; set; }
    }

    public interface IServiceRequestHandler<in TRequest, TResponse>
        where TRequest : IServiceRequest<TResponse>
    {
        Task<TResponse> Execute(string command, IRemote remote, CancellationToken cancellationToken);
    }

    public abstract class ServiceRequestHandler<TRequest, TResponse> : IServiceRequestHandler<TRequest, TResponse>
        where TRequest : IServiceRequest<TResponse>
    {
        Task<TResponse> IServiceRequestHandler<TRequest, TResponse>.Execute(string command, IRemote remote, CancellationToken cancellationToken)
            => Execute(command, remote);

        protected abstract Task<TResponse> Execute(string command, IRemote remote);
    }

    public interface IRemoteRequest<TResponse, TRemote>
    {
        TRemote Remote { get; set; }
        IServiceRequest<TResponse> Request { get; set; }
    }

    public abstract class RemoteRequestWrapper<TResponse, TRemote> : IRemoteRequest<TResponse, TRemote>
    {
        public virtual TRemote Remote { get; set; }
        public virtual IServiceRequest<TResponse> Request { get; set; }
    }
}
