﻿using Microsoft.AspNetCore.Builder;
using System.IO;

namespace AS.Omni.Host
{
    public static class HostConfigure
    {
        public static IApplicationBuilder ConfigureHost(this IApplicationBuilder app, HostSettings settings)
        {
            app.UseRouting();

            if (settings.ForceHttps) app.UseHttpsRedirection();
            
            //app.UseAuthorization();
            app.UseDefaultFiles();
            app.UseStaticFiles();

            return app;
        }
    }
}
