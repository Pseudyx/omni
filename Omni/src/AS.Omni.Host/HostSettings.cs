﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AS.Omni.Host
{
    sealed public class HostSettings
    {
        public HostSettings()
        {
            SSLCert = new SSLCert();
            Protocols = new Protocols();
        }
        public bool ForceHttps { get; set; } = true;

        public int HttpPort { get; set; } = 5000;
        public int HttpsPort { get; set; } = 5001;
        public SSLCert SSLCert { get; set; }
        public Protocols Protocols { get; set; }
    }

    public sealed class SSLCert
    {
        public string Path { get; set; } = "dev.pfx";
        public string Password { get; set; } = "developer";
    }

    public sealed class Protocols
    {
        public bool REST { get; set; } = true;
        public bool WebSocket { get; set; } = false;
        public bool gRPC { get; set; } = false;
    }
}
