﻿using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace AS.Omni.Host
{
    using Broker;
    using Grpc;
    using Web;

    public class Startup
    {
        private readonly IConfiguration _config;
        private readonly HostSettings _webHost = new HostSettings();
        private ILogger<Startup> _logger;
        public ILifetimeScope AutofacContainer { get; private set; }
        public Startup(IConfiguration config)
        {
            _config = config;
            _config.GetSection("WebHost").Bind(_webHost);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddBroker();
            services.AddOptions();

            if (_webHost.Protocols.REST) services.AddOmniControllers();
            if (_webHost.Protocols.WebSocket) services.AddSignalR();
            if (_webHost.Protocols.gRPC) services.AddGrpc();
            //services.AddNewtonsoftJson();

            services.AddMvc(option => option.EnableEndpointRouting = false).AddControllersAsServices();
                

        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new WebModule());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            _logger = logger;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.OmniWebConfigure(logger);
            app.ConfigureHost(_webHost);
            
            
            if (_webHost.Protocols.REST) app.OmniRestConfigure(logger);
            if (_webHost.Protocols.WebSocket) app.OmniSignalrConfigure(logger);
            if (_webHost.Protocols.gRPC) app.OmniGrpcConfigure(logger);
        }
    }
}
