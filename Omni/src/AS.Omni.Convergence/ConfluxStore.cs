﻿using System.Collections.Generic;
using System.IO;

namespace AS.Omni.Convergence
{
    using AS.Omni.Utility;
    using Utility;
    public class ConfluxStore<TConfigItem>
    {
        private ConfigFile<TConfigItem> _configFile;
        private readonly string _fileName;

        public ConfluxStore(string fileName = "omniConfig.json")
        {
            _fileName = Path.Combine(Directory.GetCurrentDirectory(), fileName);
            _configFile = JsonFile.Read<ConfigFile<TConfigItem>>(_fileName);
        }

        public ICollection<TConfigItem> Conflux
        {
            get { return _configFile?.Conflux; }
        }

        public void AddConflux(TConfigItem item)
        {
            _configFile?.Conflux.Add(item);
        }

        public void SaveChanges()
        {
            JsonFile.Save(_fileName, _configFile);
        }
    }

    public class ConfigFile<TList>
    {
        public List<TList> Conflux;
    }
}
