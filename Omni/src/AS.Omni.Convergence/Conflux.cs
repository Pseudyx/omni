﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AS.Omni.Convergence
{
    using Models;
    using Interfaces;
    using AS.Omni.Convergence.Remote;
    using Newtonsoft.Json;
    using System.Linq;
    using Business;

    public class Conflux : IConflux
    {
        private readonly IConfluxConfig<Models.Remote> _config;
        private readonly ITransform _transform;
        private readonly IServiceFactory _serviceFac;

        public Conflux(IConfluxConfig<Models.Remote> config, ITransform transform, IServiceFactory serviceFac)
        {
            _config = config;
            _transform = transform;
            _serviceFac = serviceFac;
        }

        public async IAsyncEnumerable<JObject> Ingress(RequestEnvelope request, [EnumeratorCancellation]CancellationToken cancellationToken)
        {
            var inlet = request.Conflux?.ToUpper() ?? _config.Conflux.FirstOrDefault(x => x.Default == true)?.Name.ToUpper();
            var conflux = _config.Conflux.Where(x => x.Name.ToUpper() == inlet).FirstOrDefault();
            
            if (conflux != null)
            {                
                var tasks = new List<Task<JObject>>();
                await conflux.Remotes.ForEachAsync(async remote =>
                {
                    tasks.Add(Task.Run(
                        async () =>
                        {
                            var remoteRequest = _transform.Transform(remote.Map.Request, request.Data);
                            var remoteResponse = await _serviceFac.Send(new ServiceRequest(remote, remoteRequest), cancellationToken);
                            return JsonConvert.DeserializeObject<JObject>(_transform.Transform(remote.Map.Response, remoteResponse));
                        }));
                });
                
                foreach (var awaiter in Combinator.Interleaved(tasks))
                {
                    var task = await awaiter;
                    var result = await task;
                    yield return result;
                }
            }

            
        }

        public class ServiceRequest : RemoteRequestWrapper<JObject, Models.Remote>
        {
            public ServiceRequest(Models.Remote remote, string request)
            {
                Remote = remote;
                Request = new Payload { Command = request };
            }

            public override Models.Remote Remote { get => base.Remote; set => base.Remote = value; }
            public override IServiceRequest<JObject> Request { get => base.Request; set => base.Request = value; }
        }

        public class Payload : IServiceRequest<JObject>
        {
            public string Command { get; set; }
        }
    }
}
