﻿
using System;

namespace AS.Omni.Convergence
{
    using AS.Omni.Config;
    using Business;
    using Interfaces;
    using System.Collections.Generic;

    public class ConfluxConfig: IDisposable, IConfluxConfig<Models.Remote>
    {
        private ConfluxStore<RemoteMapper> _store;
        public IEnumerable<Config<Models.Remote>> Conflux { get => _store.Conflux; }

        public ConfluxConfig()
        {
            _store = new ConfluxStore<RemoteMapper>();
        }

        public void AddConflux(RemoteMapper item)
        {
            _store.AddConflux(item);
        }

        public void Save()
        {
            _store.SaveChanges();
        }

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    //this.SaveChanges();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

    }
}
