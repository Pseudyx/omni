﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AS.Omni.Convergence.Utility
{
    public static class JsonEscapeFilter
    {
        public static string Json(this string input)
        {
            string escape = null;
            if (input == null)
                return input;
            foreach (var item in input)
            {
                switch (item)
                {
                    case '\"':
                        escape += "\\" + item;
                        break;
                    case '\\':
                        escape += "\\" + item;
                        break;
                    case '/':
                        escape += "\\" + item;
                        break;
                    case '\b':
                        escape += "\\b";
                        break;
                    case '\f':
                        escape += "\\f";
                        break;
                    case '\n':
                        escape += "\\n";
                        break;
                    case '\r':
                        escape += "\\r";
                        break;
                    case '\t':
                        escape += "\\t";
                        break;
                    default:
                        escape += item;
                        break;
                }
            }
            return escape;
        }
    }
}
