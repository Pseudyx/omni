﻿using Newtonsoft.Json.Linq;

namespace AS.Omni.Convergence.Interfaces
{
    public interface ITransform
    {
        string Transform(string templateString, JObject input);
        string Transform(string templateString, string input);
    }
}
