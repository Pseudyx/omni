﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AS.Omni.Convergence.Interfaces
{
    using AS.Omni.Config;
    using Omni.Interfaces;
    public interface IConfluxConfig<TRemote>
        where TRemote : IRemote
    {
        IEnumerable<Config<TRemote>> Conflux { get; }
        void Save();
    }
}
