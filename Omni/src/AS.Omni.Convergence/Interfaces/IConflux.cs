﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace AS.Omni.Convergence.Interfaces
{
    public interface IConflux
    {
        IAsyncEnumerable<JObject> Ingress(RequestEnvelope request, [EnumeratorCancellation] CancellationToken cancellationToken = default);
    }
}