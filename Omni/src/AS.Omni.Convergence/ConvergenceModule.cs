﻿using AS.Omni.Convergence.Interfaces;
using AS.Omni.Convergence.Remote;
using Autofac;
using System.Reflection;

namespace AS.Omni.Convergence
{
    public class ConvergenceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Other types registration
            //...
            builder.RegisterAssemblyTypes(typeof(ConvergenceModule).Assembly).AsImplementedInterfaces();

            builder.RegisterType<Conflux>().As<IConflux>().InstancePerDependency();

            builder.RegisterType<ConfluxConfig>().As<IConfluxConfig<Models.Remote>>().InstancePerLifetimeScope();
            

            // Register sub modules
            builder.RegisterModule(new RemoteModule());

        }
    }
}
