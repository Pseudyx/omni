﻿using DotLiquid;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using System.IO;
using System.Reflection;

namespace AS.Omni.Convergence.Business
{
    using AS.Omni.Convergence.Utility;
    using Interfaces;
    public class Transformer : ITransform
    {

        public string Transform(string templateString, string input)
        {
            return string.IsNullOrEmpty(templateString)
                ? input
                : Transform(templateString, JsonConvert.DeserializeObject(input) as JObject);

        }

        public string Transform(string templateString, JObject input)
        {
            return string.IsNullOrEmpty(templateString)
                ? input.ToString()
                : Transform(Template.Parse(templateString), input);
        }

        private string Transform(Template template, JObject input)
        {
            Hash inputvariables = Hash.FromDictionary(input.ToDictionary());
            var result = template.Render(new RenderParameters(CultureInfo.CurrentCulture) { LocalVariables = inputvariables, Filters = new Type[] { typeof(JsonEscapeFilter) } });
            Console.WriteLine(result);
            return result;
        }

        public static string GetTemplateStringFromFile(string filePath)
        {
            var path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), filePath);
            return File.ReadAllText(path);
        }

        public static Template GetTemplateFromFile(string filePath)
        {
            var template = GetTemplateStringFromFile(filePath);
            return Template.Parse(template);
        }

    }
}
