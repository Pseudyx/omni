﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AS.Omni.Convergence.Business
{
    using AS.Omni.Utility;
    using Config;
    using Models;
    using Utility;

    public class RemoteMapper : Config<Remote>
    {
        private readonly string _dataDir = Path.Combine(Directory.GetCurrentDirectory(), "Map");
        public RemoteMapper() : base()
        { }

        public override void AddRemote(Remote remote)
        {
            MapToFile(ref remote);
            Remotes.Add(remote);
        }

        public override List<Remote> Remotes
        {
            get => (base.Remotes?.Count > 0)
                    ? MapFromFile(base.Remotes).ToList()
                    : base.Remotes;
            set => base.Remotes = value;
        }

        private void MapToFile(ref Remote remote)
        {
            string reqFile = $"{remote.Name}.rqst";
            string reqPath = Path.Combine(_dataDir, reqFile);
            var req = JsonConvert.DeserializeObject<JObject>(remote.Map.Request);

            JsonFile.Save(reqPath, req);

            remote.Map.Request = reqPath;

            string respFile = $"{remote.Name}.resp";
            string respPath = Path.Combine(_dataDir, respFile);
            var res = JsonConvert.DeserializeObject<JObject>(remote.Map.Response);

            JsonFile.Save(respPath, res);

            remote.Map.Response = respPath;
        }

        private IEnumerable<Remote> MapFromFile(IList<Remote> remotes)
        {
            foreach (var remote in remotes)
            {
                string reqFile = $"{remote.Name}.rqst";
                string reqPath = Path.Combine(_dataDir, reqFile);

                remote.Map.Request = JsonFile.Read(reqPath);

                string respFile = $"{remote.Name}.resp";
                string respPath = Path.Combine(_dataDir, respFile);

                remote.Map.Response = JsonFile.Read(respPath);

                yield return remote;
            }
        }
    }
}
