//using System.Security.Cryptography.X509Certificates;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Container = Microsoft.Extensions.Hosting;

namespace AS.Omni.Server
{
    using Config;
    using Host;
    using Worker;
    public class Program
    {
        static IConfiguration _config = new ConfigurationBuilder()
          .AddJsonFile("appsettings.json", true, true)
          .Build();

        private static HostSettings _webHost = new HostSettings();
        public static async Task Main(string[] args)
        {
            _config.GetSection("WebHost").Bind(_webHost);

            if (!args.Contains("-ws"))
            {
                Console.Write("Run Worker Services? [Y/N]: ");
                var srv = Console.ReadLine();
                if (srv.ToUpper() == "Y")
                {
                    var cmds = new List<string>();
                    cmds.Add("-ws");
                    args = cmds.ToArray();
                }
            }

            //// Create a collection object and populate it using the PFX file
            //X509Certificate2Collection collection = new X509Certificate2Collection();
            //collection.Import(_sslCert.Path, _sslCert.Password, X509KeyStorageFlags.PersistKeySet);

            //Console.WriteLine("Server certificates");
            //foreach (X509Certificate2 cert in collection)
            //{
            //    Console.WriteLine("Subject is: '{0}'", cert.Subject);
            //    Console.WriteLine("Issuer is:  '{0}'", cert.Issuer);

            //    // Import the certificates into X509Store objects
            //}

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Container.Host.CreateDefaultBuilder(args)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((context, config) =>
                {
                    // Configure the root app here
                    config.SetBasePath(Directory.GetCurrentDirectory());
                    config.AddJsonFile(
                        "appsettings.json", optional: false, reloadOnChange: true);
                })
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                    .ConfigureKestrel(serverOptions =>
                    {
                        // Set properties and call methods on options
                        serverOptions.ListenAnyIP(_webHost.HttpPort);
                        serverOptions.ListenAnyIP(_webHost.HttpsPort,
                            listenOptions =>
                            {
                                listenOptions
                                .UseHttps(_webHost.SSLCert.Path, _webHost.SSLCert.Password)
                                .Protocols = Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http1AndHttp2;
                            });
                    })
                    .UseStartup<Startup>();
                })
                .UseOSService()
                .AddWorkerServices(args);

    }

    public static class OSServices
    {
        public static IHostBuilder UseOSService(this IHostBuilder builder)
        {
            if ((Debugger.IsAttached || RuntimeInformation.IsOSPlatform(OSPlatform.Windows)))
            {
                Console.WriteLine("Attached to Win Services");
                builder.UseWindowsService();
            }
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                Console.WriteLine("Attached to Systemd");
                builder.UseSystemd();
            }

            return builder;
        }
    }
}
