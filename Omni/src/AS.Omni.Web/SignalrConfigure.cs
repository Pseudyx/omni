﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;

namespace AS.Omni.Web
{
    using Hubs;


    public static class SignalrConfigure
    {
        public static IApplicationBuilder OmniSignalrConfigure(this IApplicationBuilder app, ILogger logger)
        {
            logger.LogInformation("WebSocket Hubs Enabled");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<ConvergeHub>("/hubs/converge");
            });

            return app;
        }
    }
}
