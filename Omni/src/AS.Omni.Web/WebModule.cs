﻿using Autofac;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Autofac.Integration.WebApi;

namespace AS.Omni.Web
{
    using AS.Omni.Convergence;
    

    public class WebModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Other types registration
            //...
            builder.RegisterAssemblyTypes(typeof(WebModule).Assembly).AsImplementedInterfaces();


            // Register sub modules
            builder.RegisterModule(new ConvergenceModule());

        }
    }
}
