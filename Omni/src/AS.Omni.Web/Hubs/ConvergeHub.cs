﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AS.Omni.Web.Hubs
{
    using Convergence.Interfaces;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class ConvergeHub : Hub
    {
        private readonly IConflux _conflux;
        public ConvergeHub(IConflux conflux)
        {
            _conflux = conflux;
        }

        public async Task SendMessage(string conflux, string data)
        {
            RequestEnvelope request = new RequestEnvelope { Conflux = conflux, Data = JsonConvert.DeserializeObject<JObject>(data) };
            await foreach (var response in _conflux.Ingress(request))
            {
                await Clients.Caller.SendAsync("ReceiveMessage", conflux, JsonConvert.SerializeObject(response));
            }
        }
    }
}
