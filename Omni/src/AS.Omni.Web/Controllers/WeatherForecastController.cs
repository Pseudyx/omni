﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AS.Omni.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }

//    public static class datetimeExtension
//    {
//        public static Google.Protobuf.WellKnownTypes.Timestamp toTimestamp(this DateTime datetime)
//        {
//            var dtKind = DateTime.SpecifyKind(datetime, DateTimeKind.Local);
//            DateTimeOffset localTime = dtKind;
//            var unixTimeMS = localTime.ToUnixTimeMilliseconds();
//            var seconds = unixTimeMS / 1000;
//            var nanos = (int)((unixTimeMS % 1000) * 1e6);

//            return new Google.Protobuf.WellKnownTypes.Timestamp()
//            {
//                Seconds = seconds,
//                Nanos = nanos
//            };
//        }
//    }
}

