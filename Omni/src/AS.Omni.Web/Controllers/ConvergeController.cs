﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace AS.Omni.Web.Controllers
{
    using Convergence.Interfaces;

    [ApiController]
    [Route("api/[controller]")]
    public class ConvergeController : ControllerBase
    {
        private readonly IConflux _conflux;
        public ConvergeController(IConflux conflux)
        {
            _conflux = conflux;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RequestEnvelope request)
        {
            var result = new ResponseEnvelope<JObject>();

            await foreach (var response in _conflux.Ingress(request))
            {
                result.Results.Add(response);
            }

            return (result != null) ? (IActionResult)Ok(result) : (IActionResult)BadRequest();
        }
    }
}