﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace AS.Omni.Web
{
    using AS.Omni.Convergence;
    using Controllers;
    using Microsoft.Extensions.Logging;

    public static class RestConfigure
    {
        public static IApplicationBuilder OmniRestConfigure(this IApplicationBuilder app, ILogger logger)
        {
            logger.LogInformation("REST API Enabled");

            app.UseMvc();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });

            return app;
        }

        public static IServiceCollection AddOmniControllers(this IServiceCollection services)
        {
            //Add your assembly to the ASP.NET application parts
            var builder = services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.Formatting = new Formatting();
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                }); 

            builder.AddApplicationPart(typeof(RestConfigure).Assembly);
            builder.AddControllersAsServices();

            return services;
        }
    }
}
