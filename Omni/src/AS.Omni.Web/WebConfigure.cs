﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using System.IO;

namespace AS.Omni.Host
{
    public static class WebConfigure
    {
        public static IApplicationBuilder OmniWebConfigure(this IApplicationBuilder app, ILogger logger)
        {

            var webRoot = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
            if (!Directory.Exists(webRoot))
            {
                logger.LogInformation("Initialise Web");

                Directory.CreateDirectory(webRoot);
                var assembly = typeof(WebConfigure).Assembly;
                var resourceName = $"{assembly.GetName().Name}.wwwroot.index.html";
                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                using (StreamReader reader = new StreamReader(stream))
                {
                    var file = reader.ReadToEnd();
                    File.WriteAllText($"{webRoot}\\index.html", file);
                    
                    logger.LogInformation("Generate Splash");
                }
            }

            return app;
        }
    }
}
