﻿using Newtonsoft.Json.Linq;

namespace AS.Omni
{
    using Broker;
    /// <summary>
    /// Search Criteria
    /// </summary>
    public class RequestEnvelope : IRequest<ResponseEnvelope<JObject>>
    {
        /// <summary>
        /// Provider config
        /// </summary>
        public string Conflux;

        /// <summary>
        /// Request payload data
        /// </summary>
        public JObject Data;

    }
}
