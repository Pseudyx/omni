﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Reflection;

namespace AS.Omni.Utility
{
    public static class JsonFile
    {
        public static void Save<T>(string path, T content)
        {

            var dir = new FileInfo(path).Directory?.FullName ?? AssemblyDirectory;
            DirectoryCheck(dir);

            File.WriteAllText(path, JsonConvert.SerializeObject(content));
        }

        public static T Read<T>(string path)
        {
            return JsonConvert.DeserializeObject<T>(Read(path));
        }

        public static string Read(string path)
        {
            DirectoryCheck(new FileInfo(path).Directory.FullName);

            using (var fileStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite))
            using (var textReader = new StreamReader(fileStream))
            {
                return textReader.ReadToEnd();
            }
        }

        public static void Delete(string path)
        {
            var fileInfo = new FileInfo(path);
            if (fileInfo.Exists)
            {
                File.Delete(path);
            }
        }

        private static void DirectoryCheck(string dir)
        {
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

        }

        private static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
    }
}
