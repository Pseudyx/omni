﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AS.Omni.Models
{
    public class Header
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
