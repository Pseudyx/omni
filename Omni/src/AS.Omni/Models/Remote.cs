﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AS.Omni.Models
{
    using Interfaces;
    public class Remote : IRemote
    {
        public string Name { get; set; }
        public RemoteType Type { get; set; }
        public string MediaType { get; set; }
        public string UriBase { get; set; }
        public string EndPoint { get; set; }
        public HttpVerb HttpVerb { get; set; }
        public string SslCertificate { get; set; }
        public string CertificatePassword { get; set; }
        public List<Header> Headers { get; set; }
        public Authentication Authentication { get; set; }

        [JsonIgnore]
        public TransformMap Map { get; set; } = new TransformMap();

        [JsonIgnore]
        public Uri BaseAddress
        {
            get
            {

                return new Uri(UriBase);
            }
        }
        [JsonIgnore]
        public Uri RequestUri
        {
            get
            {

                return new Uri(BaseAddress, this.EndPoint);
            }
        }
    }

    public enum RemoteType
    {
        REST,
        SOAP
    }

    public enum HttpVerb
    {
        GET,
        POST,
        PUT
    }
}
