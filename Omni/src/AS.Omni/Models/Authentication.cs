﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AS.Omni.Models
{
    using Interfaces;
    public class Authentication : IAuthentication
    {
        public AuthType Type { get; set; }
        public string Uri { get; set; }
        [JsonIgnore]
        public Uri AuthUri
        {
            get
            {
                return new Uri(Uri);
            }
        }
        public string Username { get; set; }
        public string Password { get; set; }
        public Dictionary<string, string> APIKeys { get; set; }

    }

    public enum AuthType
    {
        None,
        Basic,
        Bearer,
        APIKeys,
        Override

    }
}
