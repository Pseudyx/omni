﻿using System;
using System.Threading.Tasks;

namespace AS.Omni
{
    /// <summary>
    /// Represents void type
    /// </summary>
    public struct Empty : IEquatable<Empty>, IComparable<Empty>, IComparable
    {
        public static readonly Empty Value = new Empty();
        public static readonly Task<Empty> Task = System.Threading.Tasks.Task.FromResult(Value);
        public int CompareTo(Empty other) => 0;
        int IComparable.CompareTo(object obj) => 0;
        public override int GetHashCode() => 0;
        public bool Equals(Empty other) => true;
        public override bool Equals(object obj) => obj is Empty;
        public static bool operator ==(Empty first, Empty second) => true;
        public static bool operator !=(Empty first, Empty second) => false;
        public override string ToString() => "()";
    }
}
