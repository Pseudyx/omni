﻿using System.Threading;
using System.Threading.Tasks;

namespace AS.Omni.Broker
{
    public interface IBroker
    {
        /// <summary>
        /// send request to handler async
        /// </summary>
        /// <typeparam name="TResponse">Response type</typeparam>
        /// <param name="request">Request object</param>
        /// <param name="cancellationToken">Optional cancellation token</param>
        /// <returns>handler response as task result</returns>
        Task<TResponse> Send<TResponse>(IRequest<TResponse> request, CancellationToken cancellationToken = default);

        //TODO: Add Broadcast (send notification messages)
    }
}
