﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace AS.Omni.Broker
{
    public class BrokerServiceConfiguration
    {
        public Type MediatorImplementationType { get; private set; }
        public ServiceLifetime Lifetime { get; private set; }

        public BrokerServiceConfiguration()
        {
            MediatorImplementationType = typeof(Broker);
            Lifetime = ServiceLifetime.Scoped;
        }

        public BrokerServiceConfiguration Using<TMediator>() where TMediator : IBroker
        {
            MediatorImplementationType = typeof(TMediator);
            return this;
        }

        public BrokerServiceConfiguration AsSingleton()
        {
            Lifetime = ServiceLifetime.Singleton;
            return this;
        }

        public BrokerServiceConfiguration AsScoped()
        {
            Lifetime = ServiceLifetime.Scoped;
            return this;
        }

        public BrokerServiceConfiguration AsTransient()
        {
            Lifetime = ServiceLifetime.Transient;
            return this;
        }
    }
}
