﻿namespace AS.Omni.Broker
{
    /// <summary>
    /// Marker for request with void response
    /// </summary>
    public interface IRequest : IRequest<Empty> { }

    /// <summary>
    /// Marker for request with a generic response
    /// </summary>
    /// <typeparam name="TResponse">Response type</typeparam>
    public interface IRequest<out TResponse> : IBaseRequest { }

    /// <summary>
    /// base marker for objects implementing IRequest or IRequest{TResponse}
    /// </summary>
    public interface IBaseRequest { }
}
