﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AS.Omni.Broker
{
    public interface IRequestHandler<in TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        /// <summary>
        /// Handle request
        /// </summary>
        /// <param name="request">The request</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>Response from the request</returns>
        Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken);
    }

    /// <summary>
    /// handler request with a void response
    /// </summary>
    /// <typeparam name="TRequest">request type for handler</typeparam>
    public interface IRequestHandler<in TRequest> : IRequestHandler<TRequest, Empty>
        where TRequest : IRequest<Empty>
    {
    }
}
