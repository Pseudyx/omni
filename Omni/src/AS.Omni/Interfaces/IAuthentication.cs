﻿using System;
using System.Collections.Generic;

namespace AS.Omni.Interfaces
{
    using Models;
    public interface IAuthentication
    {
        Dictionary<string, string> APIKeys { get; set; }
        Uri AuthUri { get; }
        string Uri { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        AuthType Type { get; set; }
    }
}