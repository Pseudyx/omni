﻿using System;
using System.Collections.Generic;

namespace AS.Omni.Interfaces
{
    using Models;
    public interface IRemote
    {
        Authentication Authentication { get; set; }
        Uri BaseAddress { get; }
        string CertificatePassword { get; set; }
        string EndPoint { get; set; }

        public HttpVerb HttpVerb { get; set; }
        List<Header> Headers { get; set; }
        string MediaType { get; set; }
        string Name { get; set; }
        Uri RequestUri { get; }
        string SslCertificate { get; set; }
        RemoteType Type { get; set; }
        string UriBase { get; set; }
    }
}