﻿using System.Collections.Generic;
using System.Linq;

namespace Newtonsoft.Json.Linq
{
    public static class JsonExtensions
    {
        public static Dictionary<string, object> ToDictionary(this JObject jObject)
        {
            var dictionary = new Dictionary<string, object>();
            jObject.Properties().ForEach(item => dictionary.Add(item.Name, Convert(item)));
            return dictionary;
        }

        private static object Convert(JToken property)
        {
            switch (property.Type)
            {
                case JTokenType.Property:
                    return Convert(((JProperty)property).Value);
                case JTokenType.Array:
                    var j = new List<object>();
                    j.AddRange(((JArray)property).Select(item => Convert(item)));
                    return j;
                case JTokenType.Object:
                    return ((JObject)property).ToDictionary();
                default:
                    return (property as JValue).Value;

            }
        }
    }
}
