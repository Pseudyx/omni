﻿using System.Threading.Tasks;

namespace System.Collections.Generic
{
    public static class CollectionExtensions
    {

        #region IEnumerable Extensions
        public static void ForEach<T>(this IEnumerable<T> sequence, Action<T> action)
        {
            // argument null checking omitted
            foreach (T item in sequence) action(item);
        }

        public static async Task ForEachAsync<T>(this IEnumerable<T> sequence, Func<T, Task> action)
        {
            // argument null checking omitted
            foreach (T item in sequence) await action(item);
        }
        #endregion

        public static void AddRange<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, IEnumerable<KeyValuePair<TKey, TValue>> collection)
        {
            // argument null checking omitted
            foreach (KeyValuePair<TKey, TValue> item in collection) dictionary.Add(item.Key, item.Value);
        }


    }


}
