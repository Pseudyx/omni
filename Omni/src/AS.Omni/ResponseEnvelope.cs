﻿using System.Collections.Generic;

namespace AS.Omni
{
    public class ResponseEnvelope<TResult>
    {
        public int Count { get => Results.Count; }

        public IList<TResult> Results;

        public ResponseEnvelope()
        {
            Results = new List<TResult>();
        }
    }
}
