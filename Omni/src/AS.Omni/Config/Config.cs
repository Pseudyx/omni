﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AS.Omni.Config
{
    using Omni.Interfaces;
    public abstract class Config<TRemote> : IConfig<TRemote> where TRemote : IRemote
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Default { get; set; }

        public virtual List<TRemote> Remotes { get; set; } = new List<TRemote>();

        public virtual void AddRemote(TRemote remote)
        {
            Remotes.Add(remote);
        }

        public Config()
        {
            Remotes = Remotes ?? new List<TRemote>();
        }
    }
}
