﻿using System.Collections.Generic;
using AS.Omni.Interfaces;

namespace AS.Omni.Config
{
    public interface IConfig<TRemote> where TRemote : IRemote
    {
        bool Default { get; set; }
        string Description { get; set; }
        string Name { get; set; }
        List<TRemote> Remotes { get; set; }

        void AddRemote(TRemote remote);
    }
}