﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace AS.Omni.Grpc
{
    public static class GrpcConfigure
    {
        public static IApplicationBuilder OmniGrpcConfigure(this IApplicationBuilder app, ILogger logger)
        {
            logger.LogInformation("gRPC Enabled");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<GreeterService>();

                endpoints.MapGet("/grpc", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client.");
                });
            });

            return app;
        }
    }
}
