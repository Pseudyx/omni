﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;

namespace AS.Omni.Worker
{
    public static class WorkerServices
    {
        public static IHostBuilder AddWorkerServices(this IHostBuilder builder, string[] args) =>
            builder.ConfigureServices((hostContext, services) =>
            {
                if (args.Contains("-ws")) { services.AddHostedService<Worker>(); }
            });
    }
}
